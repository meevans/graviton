# How-to-rediscover-the-Higgs 
## Jupyter notebook with steps to rediscover the Higgs boson yourself!
------

## Get Started (online)
Click on this link ---> [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.cern.ch%2Fmeevans%2Fgraviton/master)

Click on How-to-rediscover-the-Higgs.ipynb


## Get Started (on your own laptop)
On a terminal type "git clone https://gitlab.cern.ch/meevans/how-to-rediscover-the-higgs-13tev.git"

Open a [Jupyter notebook](https://jupyter.org) using your favourite [Python](https://www.python.org) (3.6 or above) environment (mine is [Anaconda](https://www.anaconda.com/distribution/))

Click on How-to-rediscover-the-Higgs.ipynb (either the original one you downloaded or your version that you downloaded)


![HZZ Feynman diagram](HZZ_feynman.png)
